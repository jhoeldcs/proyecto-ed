package clases;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/*
*
* @author Rafael
* @param 
* 
*/

public class Raqueta {

    private int x, y;
    static final int ANCHO = 10, ALTO = 90;

    public Raqueta(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Rectangle2D getRaqueta() {
        return new Rectangle2D.Double(x, y, ANCHO, ALTO);
    }
//Raqueta para jugador 1
    public void moverR1(Rectangle limites) {
        if (EventoTeclado.w && y > limites.getMinY()) {
            y--;
        }
        if (EventoTeclado.s && y < limites.getMaxY()-ALTO) {
            y++;
        }
    }
//Raqueta para jugador 2
    public void moverR2(Rectangle limites) {
        if (EventoTeclado.up && y > limites.getMinY()) {
            y--;
        }
        if (EventoTeclado.down && y < limites.getMaxY()-ALTO) {
            y++;
        }
    }
    
    public int metodo_para_pruebas_unitarias(int h) {
    	if (h < 0) {
    		h = h * (-1);
    	}
    	
    	if (this.x > h && this.y > h) {
    		h = h * 2;
    	}
    	
    	while (h < this.x) {
    		h=h+this.y;
    	}
    	
    	return h;
    }
}
